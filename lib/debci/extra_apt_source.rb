module Debci
  class ExtraAptSource
    attr_reader :entry

    def initialize(entry, allowed_users = [], signing_key = nil)
      @entry = entry
      @allowed_users = allowed_users || []
      @signing_key = signing_key
    end

    def allowed?(user)
      @allowed_users.include?(user.id) || @allowed_users.empty?
    end
  end
end
