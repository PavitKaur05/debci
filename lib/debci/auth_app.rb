require 'debci/app'
require 'debci/user'

module Debci
  class AuthApp < Debci::App
    enable :sessions
    set :session_secret, Debci.config.session_secret || SecureRandom.hex(64)

    def authenticate!
      @user = Debci::User.find(session[:user_id])
    rescue ActiveRecord::RecordNotFound
      session[:original_url] = request.path
      redirect('/user/login')
      halt
    end

    def forbidden
      halt 403, erb(:forbidden)
    end
  end
end
